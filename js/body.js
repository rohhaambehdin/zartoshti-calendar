let popoverList

$(document).ready(function() {
  	const tarsaiiMonthNames = ['ژانویه', 'فوریه', 'مارس', 'آپریل', 'می', 'ژوئن', 'جولای', 'اوت', 'سپتامبر', 'اکتبر', 'نوامبر', 'دسامبر']
    let today = Jalaali2Zartoshti(toJalaali(new Date()));
    const menus = ['calendar', 'events', 'notes']
  	let currentMonth = today.zm;
    let currentYear = today.zy - 7180;
    let currentMenu = menus[0]
    const eventDays = {
        1: {
            1: ['روز اورمزد - جشن نوروز جشن جمشیدشاه'],
            2: ['روز بهمن - جشن نوروز'],
            3: ['روز اردیبهشت - جشن نوروز'],
            4: ['روز شهریور - جشن نوروز'],
            6: ['روز خرداد - زادروز اشو زرتشت اسپندمان'],
            13: ['روز تیر - سیزده بدر'],
            17: ['روز سروش - جشن سروش‌گان'],
            19: ['روز فروردین - جشن فروردینگان'],
            25: ['روز ارد - بزرگداشت عطار']
        },
        2: {
            3: ['روز اردیبهشت - جشن اردیبهشتگان'],
            10: ['روز آبان - جشن چلمو'],
            15: ['روز دی به مهر - گهنبار نخست میدیوزرم'],
            24: ['روز دین - زادروز خردکام کیخسرو آرش گرگین'],
            25: ['روز بزرگداشت فردوسی بزرگ'],
            28: ['روز زامیاد - بزرگداشت مغ نیشابور خیام']
        },
        3: {
            6: ['روز خرداد - جشن خردادگان'],
            29: ['روز ماراسپند - جشن عیدماه']
        },
        4: {
            10: ['روز آبان - گهنبار دوم مَدیوشم'],
            13: ['روز تیر - جشن تیرگان']
        },
        5: {
            7: ['روز امرداد - جشن امردادگان'],
            8: ['روز دی به آذر - بزرگداشت سهروردی']
        },
        6: {
            1: ['روز اورمزد - بزرگداشت پورسینا'],
            3: ['روز اردیبهشت - جشن کشمین'],
            4: ['روز شهریور - جشن شهریورگان'],
            5: ['روز سپندارمذ - بزرگداشت رازی'],
            8: ['روز دی به آذر - خزان جشن'],
            13: ['روز تیر - بزرگداشت بیرونی'],
            15: ['روز دی به مهر - بازار جشن'],
            25: ['روز ارد - گهنبار سوم پدَیشهَه']
        },
        7: {
            8: ['روز دی به آذر - بزرگداشت مولوی'],
            16: ['روز مهر - جشن مهرگان'],
            20: ['روز بهرام - بزرگداشت مغ شیراز حافظ'],
            25: ['روز ارد - گهنبار چهارم ایاسرم']
        },
        8: {
            7: ['روز امرداد - روز کوروش بزرگ'],
            10: ['روز آبان - جشن آبانگان'],
            25: ['روز ارد - نبرد تلخ قادسیه'],
        },
        9: {
            9: ['روز آذر - جشن آذرگان'],
            21: ['روز رام - نجات آذربایجان'],
            30: ['روز انغران - شب چله']
        },
        10: {
            1: ['روز اورمزد - نخستین جشن دیگان'],
            5: ['روز سپندارمذ - روز خورایزد جان‌اسپار شدن اشو زرتشت اسپندمان'],
            8: ['روز دی به آذر - دومین جشن دیگان'],
            15: ['روز دی به مهر - سومین جشن دیگان+گهنبار پنجم مدیاریم'],
            23: ['روز دی به دین - چهارمین جشن دیگان']
        },
        11: {
            2: ['روز بهمن - جشن بهمنگان'],
            5: ['روز سپندارمذ - جشن نوسره'],
            10: ['روز آبان - جشن سده'],
            25: ['روز ارد - نبرد تلخ نهاوند']
        },
        12: {
            5: ['روز سپندارمذ - جشن سپندارمذگان'],
            15: ['روز دی به مهر - روز درختکاری'],
            21: ['روز رام - بزرگداشت مغ گنجه نظامی'],
            25: ['روز ارد - سرودن اهونودگاه'],
            26: ['روز اشتاد - سرودن اشتودگاه'],
            27: ['روز آسمان - سرودن سپنتمدگاه'],
            28: ['روز زامیاد - سرودن وهوخشترگاه'],
            29: ['روز ماراسپند - سرودن وهیشتودگاه'],
            30: ['روز انارام - گهنبار ششم همس‌پت میدیم']
        }
    }

    function convertToPersianNumbers(input) {
        const persianNumbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        return input.replace(/\d/g, (digit) => persianNumbers[digit]);
    }

    function isNabor(day) {
        return day == 2 || day == 12 || day == 14 || day == 21 ? true : false
    }

  	function detectSwipe(element, callback) {
        let startX, startY, endX, endY;

        element.on("touchstart", function (event) {
            let touch = event.touches[0];
            startX = touch.clientX;
            startY = touch.clientY;
        });

        element.on("touchend", function (event) {
            let touch = event.changedTouches[0];
            endX = touch.clientX;
            endY = touch.clientY;

            let diffX = endX - startX;
            let diffY = endY - startY;

            // Determine swipe direction
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (diffX > 30) {
                    callback("right");
                } else if (diffX < -30) {
                    callback("left");
                }
            } else {
                if (diffY > 30) {
                    callback("down");
                } else if (diffY < -30) {
                    callback("up");
                }
            }
        });
    }
  
    $('#todayTitle').html(function () {
        let dayNamePart = 'روز ' + '<span class="dayName"> ' + today.zDayName;
        if (isNabor(today.zd)) {
            dayNamePart += '<span class="Nabor">امروز، روز نبُر است. در روز نبُر زرتشتیان نه دامی را می‌کشند و نه گوشت می‌خورند.</span>';
        }
        let dateRestPart = ' </span> ' + today.zd.toLocaleString("fa-ir")+ ' ماه ' + today.zMonthName +' سال ' + today.zy.toLocaleString('fa-ir', {useGrouping:false}) + ' زرتشتی';
      	const tarsaiiDate = toGregorian(currentYear, currentMonth, today.zd)
        const tarsaiiPart = `<div class="tarsaiiTitle">برابر است با ${tarsaiiDate.gd} ${tarsaiiMonthNames[tarsaiiDate.gm - 1]} ${tarsaiiDate.gy} ترسایی</div>`
        return dayNamePart + dateRestPart + tarsaiiPart;
    });

    if (isNabor(today.zd)) $('.dayName').css({color: '#69af00', cursor: 'pointer'})
    
    const calendarTemp = document.importNode(document.getElementById('calendar-temp').content, true);
    $('#template-container').append(calendarTemp)
    updateCalendar();
    updateCurrentDate();

    $('#events').click(function() {
        updateMenus(menus[0])
        $('#template-container').empty();
        const temp = document.importNode(document.getElementById('events-temp').content, true);
        $('#template-container').append(temp)
        updateEvents();
        currentMenu = menus[1]
    })

    $('#gahshomar').click(function() {
        updateMenus(menus[1])
        $('#template-container').empty();
        const temp = document.importNode(document.getElementById('calendar-temp').content, true);
        $('#template-container').append(temp)
        updateCalendar();
        currentMenu = menus[0]
    })

    $('#notes').click(function() {
        updateMenus(menus[2])
        updateNotes()
        currentMenu = menus[2]
    })

    const calendarPrev = () => {
        if (currentMonth == 1) {
            currentMonth = 12;
            currentYear--;
        } else {
            currentMonth--;
        }
        
        switch (currentMenu) {
            case menus[0]:
                updateCalendar();
                break;
            case menus[1]:
                updateEvents();
                break;
            case menus[2]:
                updateNotes();
                break;
        }
        updateCurrentDate();
    }

    const calendarNext = () => {
        if (currentMonth == 12) {
            currentMonth = 1;
            currentYear++;
        } else {
            currentMonth++;
        }
        switch (currentMenu) {
            case menus[0]:
                updateCalendar();
                break;
            case menus[1]:
                updateEvents();
                break;
            case menus[2]:
                updateNotes();
                break;
        }
        updateCurrentDate();
    }
    
    let swipeArea = $('#template-container')
    detectSwipe(swipeArea, function (direction) {
        if (direction == 'left') {
            calendarPrev()
        } else if (direction == 'right') {
            calendarNext()
        }
    });

    $('#calendar-prev').click(calendarPrev);
    $('#calendar-next').click(calendarNext);

    function updateCurrentDate() {
        const currGregorianDate = toGregorian(currentYear, currentMonth, 30)
        const currGregorianMonth =tarsaiiMonthNames[currGregorianDate.gm - 1]
        let prevGregorianMonth = tarsaiiMonthNames[11]
      	let prevGregorainYear = currGregorianDate.gy - 1

        if (currentMonth != 10) {
	          prevGregorainYear = currGregorianDate.gy
            prevGregorianMonth = tarsaiiMonthNames[currGregorianDate.gm - 2]
        }
        
        $('#calendar-current-date').html(`
            <span>${ZartoshtiMonthNameArray[currentMonth]} ${(currentYear + 7180).toLocaleString('fa-ir', { useGrouping: false })}</span>
            <span class='monthsSeparator d-none d-sm-inline-block'>-</span>
		    <span class='tarsaiiMonths'>${prevGregorianMonth} ${prevGregorainYear != currGregorianDate.gy ? prevGregorainYear : ''} / ${currGregorianMonth} ${currGregorianDate.gy}</span>
		`)
      
        $('[data-bs-toggle="tooltip"]').tooltip();
        const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
        popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
    }

    function updateCalendar() {
        const thisMonthLenght = jalaaliMonthLength(currentYear, currentMonth);
        const lastMonthsLength = jalaaliMonthLength((currentMonth == 1 ? currentYear - 1 : currentYear), (currentMonth == 1 ? 12 : currentMonth - 1));
        let gTemp = toGregorian((currentMonth == 1 ? currentYear - 1 : currentYear), (currentMonth == 1 ? 12 : currentMonth - 1), lastMonthsLength);
        const lastDayOfLastMonth = ((new Date (gTemp.gy, gTemp.gm - 1, gTemp.gd)).getDay() + 1) % 7;
        const gTemp2 = toGregorian(currentYear, currentMonth, thisMonthLenght);
        let lastDayOfMonth = ((new Date (gTemp2.gy, gTemp2.gm - 1, gTemp2.gd)).getDay() + 1) % 7;

        // clear calendar
        $('#calendar-dates').empty();

        if (lastDayOfLastMonth < 6) {
            for (let i = 0; i <= lastDayOfLastMonth; i++) {
              	const day = i + lastMonthsLength - lastDayOfLastMonth
              	const tarsaiiDate = toGregorian(currentYear, currentMonth - 1, day)
                $('<li>', {
                'class': 'calendar-date last',
                text: day.toLocaleString('fa-ir'),
                css: {color: 'gray', fontStyle: 'italic', "background-color": "#f3f3f3"}
                }).append('<div class="zdn d-none d-sm-block">' + ZartoshtiDayNameArray[i + lastMonthsLength - lastDayOfLastMonth] + '</div>')
              		.append('<div class="td">' + tarsaiiDate.gd + '</div>')
                  .appendTo('#calendar-dates');
            }
        }

        for (let i = 1; i < thisMonthLenght + 1; i++) {
            let color = 'black';
            let bgcolor = '';
            let font_weight = 'normal';
            let border = '';
            if (i == today.zd && currentMonth == today.zm && currentYear == (today.zy - 7180)) {
                bgcolor = '#ffd2cb';
                border = '3px solid #6c00d3';
            }
            if (((i + lastDayOfLastMonth) % 7) == 6) {color = 'red';} // Friday
            if (isNabor(i)) {
                bgcolor = '#69af00';
                color = 'yellow';
                font_weight = 'bolder';
            } // Nabor
            if (eventDays[currentMonth].hasOwnProperty(i)) {
                color = 'red';
            }
            const li = $('<li>', {
                'class': 'calendar-date',
                text: i.toLocaleString('fa-ir'),
                css: {
                    'color': color,
                    'font-weight': font_weight,
                    'background-color': bgcolor,
                    'border': border
                }
            })
            if (eventDays[currentMonth].hasOwnProperty(i)) {
                if ($(window).width() < 576) {
                    li.attr({'data-bs-toggle': 'popover', 'data-bs-content': eventDays[currentMonth][i]})
                } else {
                    li.attr({'data-bs-toggle': 'tooltip', 'title': eventDays[currentMonth][i]})
                }
            }
            li.append('<div class="zdn d-none d-sm-block">' + ZartoshtiDayNameArray[i] + '</div>').appendTo('#calendar-dates');
            // if (eventDays[currentMonth].hasOwnProperty(i)) {
            //     li.append(`<div class='zdn zdn-event'>${eventDays[currentMonth][i]}</div>`)
            // }
          	const tarsaiiDate = toGregorian(currentYear, currentMonth, i)
          	li.append('<div class="td">' + tarsaiiDate.gd + '</div>').appendTo('#calendar-dates');
        }
        
        if (lastDayOfMonth < 6) {
            let color = 'gray';
            for (let i = 0; i < 6 - lastDayOfMonth; i++) {
                if (i + lastDayOfMonth == 5) {color = 'lightsalmon'};
              	const tarsaiiDate = toGregorian(currentYear, currentMonth + 1, i + 1)
                $('<li>', {
                    'class': 'calendar-date next',
                    text: (i + 1).toLocaleString('fa-ir'),
                    css: {color: color, fontStyle: 'italic', "background-color": "#f3f3f3"}
                }).append('<div class="zdn d-none d-sm-block">' + ZartoshtiDayNameArray[i + 1] + '</div>')
                  .append('<div class="td">' + tarsaiiDate.gd + '</div>')
                  .appendTo('#calendar-dates');
            }
        }
    }

    function updateEvents() {
        $('#events-container').empty()
        const events = eventDays[currentMonth]
        for (let e in events) {
            if (events[e].length > 1) {
                for (let dayEvent of events[e]) {
                    const li = $('<li>')
                    const firstSpan = $('<span>', {text: `${convertToPersianNumbers(e)} ${ZartoshtiMonthNameArray[currentMonth]}`});
                    const secondSpan = $('<span>', {text: dayEvent});
                    li.append(firstSpan).append(secondSpan).appendTo('#events-container')
                }
            } else {
                const li = $('<li>')
                const firstSpan = $('<span>', {text: `${convertToPersianNumbers(e)} ${ZartoshtiMonthNameArray[currentMonth]}`});
                const secondSpan = $('<span>', {text: ` ${events[e][0]}`})
                li.append(firstSpan).append(secondSpan).appendTo('#events-container')
            }
        }
    }

    function updateNotes() {
        $('#template-container').empty();
        const temp = document.importNode(document.getElementById(`notes-${currentMonth}-temp`).content, true);
        $('#template-container').append(temp)
    }

    function updateMenus(clickedMenu) {
        $('.menu-items li a.active').removeClass('active')
        switch (clickedMenu) {
            case menus[0]:
                $('#events').addClass('active')
                break;
            case menus[1]:
                $('#gahshomar').addClass('active')
                break;
            case menus[2]:
                $('#notes').addClass('active')
                break;
        }
    }
});
